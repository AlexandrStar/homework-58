import React, { Component } from 'react';
import './App.css';
import Modal from './Components/UI/Modal/Modal';
import Button from './Components/UI/Button/Button'
import Alert from './Components/UI/Alert/Alert'

class App extends Component {

    state = {
        modalShow: false,
        dismiss: false,
    };

    modal = () => {
        this.setState({modalShow: true});
    };

    modalCancel = () => {
        this.setState({modalShow: false});
    };

    someHandler = () => {
        this.setState({dismiss: true})
    };

  render() {
    return (
      <div className="App">
          {this.state.modalShow && <Modal
              show={this.state.modalShow}
              close={this.modalCancel}
              title="Some kinda modal title">
              <p>This is modal content</p>
          </Modal>}
          <Button
              btnType="Success"
              onClick={this.modal}
          >
              SHOW MODAL
          </Button>
          <Alert
              type="warning"
              dismiss={this.someHandler}
          >This is a warning type alert</Alert>
          <Alert clickDissmisible={this.someHandler} type="success">This is a success type alert</Alert>
      </div>
    );
  }
}

export default App;
