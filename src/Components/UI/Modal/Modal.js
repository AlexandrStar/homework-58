import React, {Fragment} from 'react';
import Backdrop from "../Backdrop/Backdrop";

import './Modal.css';


const Modal = props => {
  return (
    <Fragment>
      <Backdrop onClick={props.close} show={props.show} />
      <div
        className="Modal"
        style={{
          transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: props.show ? '1' : '0'
        }}
          >
          <div className="title">
              <h3>{props.title}</h3>
              <button className="btn-close" onClick={props.close}>x</button>
          </div>
        {props.children}
      </div>
    </Fragment>
  );
};

export default Modal;