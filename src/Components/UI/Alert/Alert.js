import React, {Fragment} from 'react';
import './Alert.css';

const Alert = props => {
    return (
        <Fragment>
            <div
                onClick={props.clickDissmisible}
                className={['Alert', props.type].join(' ')}
            >
                {props.children}
                {props.dismiss !== undefined ? <button className="btn-dismiss" onClick={props.close}>X</button> : null}
            </div>
        </Fragment>
    );
};

export default Alert;